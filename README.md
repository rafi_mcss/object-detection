# object-detection

## backend
Gets an image from dart, sends it to Amazon, parses the data, sends the information with only the labels we care about to the front end.

Uses socket.io which is a bi-directional communication library based on websockets

## frontend
It connects to the backend using socket.io. It receives a message with a list of Labels, and corresponding image. Each label has a name and a bounding box.

- [ ] figure out image encoding scheme



